# Kibana
Kibana is a tooling that allows user to search, view, and interact with data stored in Elasticsearch indices. 

You can easily perform advanced data analysis and visualize your data in a variety of charts, tables, and maps.

### Key features

* interactive graphs
* free templates
* data filtering and discovering
* alert firing based on custom rules
* aids data-mining especially for analyst/data scientist that does not have profound knowledge of database query languages.
# Filebeat
* daemon that ships logs to database/transform agent (logstash)
* installed on client machine aka log origin


https://rexsoftware.atlassian.net/wiki/display/PHOENIX/Filebeat

### Installation
```
curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-5.0.0-amd64.deb
 
sudo dpkg -i filebeat-5.0.0-amd64.deb
 
 
# To make sure filebeat starts and stops automatically with the server, add its init script to the default runlevels.
sudo systemctl enable filebeat.service
```


### Configuration
```
filebeat:
  prospectors:
    - input_type: log
      paths:
        - /var/log/auth.log
        - /var/log/syslog
      document_type: syslog
 
    - input_type: log
      paths:
        - /var/www/janus/storage/logs/laravel.log
      document_type: applog
      # resource tag   
      fields:
        app: janus
output:
  logstash:
    hosts: ["logstash_server_ip:5044"]
 
    # ssl cert is not mandatory if logstash and client machine are in the same vpc, using private IP is secure enough
    ssl:
      certificate_authorities: ["/etc/pki/tls/certs/logstash-forwarder.crt"]
 
shipper:
 
logging:
  files:
    rotateeverybytes: 10485760 # = 10MB
 
name: "janus"
```


### SSL Certificate
Make sure you have already set up SSL certificate on logstash server, see *Secure Connection - SSL Certificate* in [logstash](../logstash) 

```
sudo mkdir -p /etc/pki/tls/certs
 
 
sudo mv /tmp/logstash-forwarder.crt /etc/pki/tls/certs/
```

### Filebeat Commands
```
# to start
sudo service filebeat start
 
# to restart
sudo service filebeat restart
 
# to stop
sudo service filebeat stop
 
# to check status
sudo service filebeat status
```


### Debugging
* Default filebeat log file resides in /var/log/filebeat/filebeat 
* Specify log location in /etc/filebeat/filebeat.yml
# Logstash 
* Ingest data and transform
* sends data to database, in our case Elastic Cloud
* requires java


#### Installation

##### Install Java

```
sudo add-apt-repository -y ppa:webupd8team/java
 
 
sudo apt-get update
 
 
sudo apt-get -y install oracle-java8-installer
```

##### Install Logstash

```
curl -L -O https://artifacts.elastic.co/downloads/logstash/logstash-5.0.0.deb
 
sudo dpkg -i logstash-5.0.0.deb
 
 
# To make sure logstash starts and stops automatically with the server, add its init script to the default runlevels.
sudo systemctl enable logstash.service
```

### Configure Logstash

##### Input

```
# place this in /etc/logstash/conf.d/beats-input.conf

input {
  beats {
    port => 5044
    # only include ssl_* fields if using ssl certification
    ssl => true
    ssl_certificate => "/etc/pki/tls/certs/logstash-forwarder.crt"
    ssl_key => "/etc/pki/tls/private/logstash-forwarder.key"
  }
}
```

##### Transform

```
# place this in /etc/logstash/conf.d/filter.conf

filter {
   if [type] == "applog" {
     grok {
       match => { "message" => "payload=%{GREEDYDATA:payload}" }
     }
     json {
       source => "payload"
       target => "parsed_json"
       remove_field => ["payload"]
     }
     mutate {
        add_field => {
         "payload" => "%{[parsed_json]}"
        }
     }
   }
}
```


##### Output

```
# place this in /etc/logstash/conf.d/elastic-output.conf

output {
  elasticsearch {
    hosts => "{hostname} e.g - https://c456b81707e8b45ede93f6d1ed01dc78.ap-southeast-2.aws.found.io:9243"
    user => "{username}"
    password => "{password}"
    manage_template => false
    index => "%{[@metadata][beat]}"
    document_type => "%{[@metadata][type]}"
  }
}
```


### Secure Connection - SSL Certificate
If your log origin server is not in the same VPC as your logstash server and therefore stopping your from using private ip, it is crucial to set up SSL cert to ensure secure connection.

##### Generate SSL Certificate

```
# create folders
sudo mkdir -p /etc/pki/tls/certs
 
 
sudo mkdir /etc/pki/tls/private
 
 
# config ssl cert
sudo vi /etc/ssl/openssl.cnf
 
 
# Find the [ v3_ca ] section in the file, and add this line under it (substituting in the Logstash Server's IP address):
subjectAltName = IP: Logstash_server_ip
 
 
# generate cert
cd /etc/pki/tls
sudo openssl req -config /etc/ssl/openssl.cnf -x509 -days 3650 -batch -nodes -newkey rsa:2048 -keyout private/logstash-forwarder.key -out certs/logstash-forwarder.crt
```

##### Distribute SSL Certificate to Client Machines

###### Generate ssh key in logstash server

```
# Generate ssh key on Logstash machine
ssh-keygen -t rsa

# Copy ssh pub key to your clipboard for later use, to get pub key - 
cat ~/.ssh/id_rsa.pub
```

###### Add logstash server to client machine's trusted host

```
# paste content of id_rsa.pub (from logstash server, previous step) into authorized_keys
 
vi ~/.ssh/authorized_keys
```

###### Copy SSL certificate to client machinese

```
scp /etc/pki/tls/certs/logstash-forwarder.crt ubuntu@client_server_ip:/tmp
```

### Install Plugins

##### Beats
Beat sends events to Logstash. Logstash receives these events by using the Beats input plugin for Logstash. It is recommended to update Beats plugin for Logstash from time to time

```
sudo /usr/share/logstash/bin/logstash-plugin update logstash-input-beats
sudo service logstash restart
```

##### Elasticsearch
Logstash sends the transaction to Elasticsearch by using the Elasticsearch output plugin for Logstash. It is recommended to update Elasticsearch plugin for Logstash from time to time

```
sudo /usr/share/logstash/bin/logstash-plugin update logstash-output-elasticsearch
sudo service logstash restart
```

### Logstash Commands

```
# to start
sudo service logstash start
 
# to restart
sudo service metricbeat restart
  
# to stop
sudo service metricbeat stop
  
# to check status
sudo service metricbeat status
```


### Debugging
* Default Logstash log file resides in /var/log/logstash/logstash-plain.log. 
* You can specify logout location in /etc/logstash/logstash.yml


# phoenix-logging
Docs and installation guides to all the components we have for our internal loggin system

### Logging System Architecture Diagram
* [filebeat](/filebeat)
* [logstash](/logstash)

                                                                                                                                                                                     
                                                                                                                                                                                     
```                                                                                                                                                                                     
                                                                                                                                                                                     
                                                                                             Elastic Cloud is elastic                                                                
                                                                                             search + Kibana on cloud.  ◀─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─                               
                                                                                                                                                      │                              
                                                                                                                                                                                     
                                                                                                                                                      │                              
┌───────────────────┐                                                                                                                                                                
│                   │                                                                                                                                 │                              
│        App        │                                                                                                                                                                
│           ┌───────┴──┐                                                                                                                              │                              
└───────────┤ Filebeat │─────────────────────┐                                                                          ┌──────────────────────────────────────────────────────────┐ 
            └──────────┘                     │                                                                          │Elastic Cloud                                             │ 
                                             │                                                                          │(https://53ba8d30bcf9f88827b88092dbe30e52.ap-southeast-2.a│ 
┌───────────────────┐                        │                                                                          │ws.found.io)                                              │ 
│                   │                        │                                                                          │                                                          │ 
│        App        │                        │                      ┌─────────────────────────┐                         │  ┌─────────────────────┐        ┌──────────────────────┐ │ 
│           ┌───────┴──┐                     │                      │        Logstash         │                         │  │                     │        │                      │ │ 
└───────────┤ Filebeat │─────────────────────┼──────  ships  ──────▶│      (10.0.1.131)       │────────────────────────▶│  │                     │        │                      │ │ 
            └──────────┘                     │       logs to        │                         │                         │  │                     │        │                      │ │ 
                                             │                      └─────────────────────────┘                         │  │    Elasticsearch    │        │        Kibana        │ │ 
┌───────────────────┐                        │                                   │                                      │  │                     │        │                      │ │ 
│                   │                        │                                                                          │  │                     │        │                      │ │ 
│        App        │                        │                                   │                                      │  │                     │        │                      │ │ 
│           ┌───────┴──┐                     │                                                                          │  └────────┬────────────┘        └────────────┬─────────┘ │ 
└───────────┤ Filebeat │─────────────────────┘                                   │                                      └──────────────────────────────────────────────────────────┘ 
            └──────────┘                                                                                                            │                                  │             
                  │                                                              │                                                                                                   
                                                                                                                                    │                                  │             
                  │                                                              │                                                                                                   
                                                                                                                                    │                                  │             
                  │                                                              │                                                                                                   
                                                                                                                                    │                                  │             
                  │                                                              ▼                                                  ▼                                  ▼             
                  ▼                                                                                                                                                                  
                                                                                                                                                                                     
     Filebeat is a local daemon                                     Logstash is an open source,                        Elasticsearch is a database         Kibana is a visualisation 
        that ships logs to a                                        server-side data processing                        engine that stores, search,          tool for Elasticsearch   
    destination. Filebeat can be                                    pipeline that ingests data                          and analyse data quickly.          database. You can create  
    installed on any machines -                                        from multiple source,                           It also comes with restful           graphs and then search,  
     database/application..etc                                     transforms it and send it to                        API endpoints allowing you           view, interact with the  
                                                                    your database - in our case                           to update/delete data            graph. Which is handy for 
                                                                           Elastic Cloud                                                                       data exploration      
```                                                                                                                                                                                     
                                                                                                                                                                                     

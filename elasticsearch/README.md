# Elasticsearch
Elasticsearch is a distributed, RESTful search and analytics engine that store, search, and analyze big volumes of data quickly.

### Terminology & Definition

##### Cluster

* A cluster is a collection of one or more nodes (servers) that together holds your entire data and provides federated indexing and search capabilities across all nodes.
* A cluster is identified by a unique name which by default is "elasticsearch" - This name is important because a node can only be part of a cluster if the node is set up to join the cluster by its name.

ps - we don't need to worry about cluster naming as Elastic Cloud handles that for us

##### Node

* A node is a single server that is part of your cluster, stores your data and participates in the cluster’s indexing and search capabilities.
* In a single cluster, you can have as many nodes as you want.
* There are 5 different kind of nodes: *Master Node*, *Data Node*, *Ingest Node*, *Tribe Node*, *All-around Node* 
* recommended node settings - 3 nodes consisting *2 all-around node* & *1 data node* (for < 32gb monthly log storage with consistent log traffic)

###### Master node

The master node is responsible for lightweight cluster-wide actions such as creating or deleting an index, tracking which nodes are part of the cluster, and deciding which shards to allocate to which nodes. It is important for cluster health to have a stable master node.

However, it is common to have a master/data hybrid node as instances are expensive

``` 
# node config under /etc/elasticsearch/[config_file]
node.master: true
node.data: false
node.ingest: false
```

###### Data node
Data nodes hold the shards that contain the documents you have indexed. 

Data nodes handle data related operations like CRUD, search, and aggregations. These operations are I/O-, memory-, and CPU-intensive. It is important to monitor these resources and to add more data nodes if they are overloaded.

The main benefit of having dedicated data nodes is the separation of the master and data roles.

``` 
# node config under /etc/elasticsearch/[config_file]
node.master: false
node.data: true
node.ingest: false
```
###### Ingest node

Ingest node execute pre-processing pipelines, it may make sense to have dedicated ingest node when we have resource intensive or large amount of task to process before the logs hit data node.

Ingest node is a new node type introduced in ES5, it's a cut down version of logstash. 

Benefit of using an Ingest node instead of logstash is that we don't have to worry about logstash and logstash broker, ingest node will be in our ES cluster which makes it easily scalable.
``` 
# node config under /etc/elasticsearch/[config_file]
node.master: false
node.data: false
node.ingest: true
```

###### Tribe node
Also known as the coordinating node. If you take away the ability to be able to handle master duties, to hold data, and pre-process documents, then you are left with a coordinating node that can only route requests, handle the search reduce phase, and distribute bulk indexing.

Essentially, coordinating only nodes behave as smart load balancers. Tribe node is commonly installed on the kibana machine, to serve as a load balancer that fetches data from the data nodes.

``` 
# node config under /etc/elasticsearch/[config_file]
node.master: false
node.data: false
node.ingest: true
```

###### All-around node

This node handles cluster-wide action such as indices manipulation, shard allocation, as well as document CRUD and data ingestion. All-around node will very likely be the go-to nodes for low/medium data traffic.

``` 
# node config under /etc/elasticsearch/[config_file]
node.master: true
node.data: true
node.ingest: true
```

##### Index

An index is a collection of documents that have somewhat similar characteristics. For example, you can have an index for customer data, another index for a product catalog, and yet another index for order data

##### Type

A type is a logical category of your index. For example, let’s assume you run a blogging platform and store all your data in a single index. In this index, you may define a type for user data, another type for blog data, and yet another type for comments data.

##### Document

A document is a basic unit of information that can be indexed. For example, you can have a document for a single customer, another document for a single product, and yet another for a single order.


### Shards & Replicas

##### Shards

A single index of a billion documents taking up 1TB of disk space may not fit on the disk of a single node or may be too slow to serve search requests from a single node alone - Elasticsearch provides the ability to subdivide your index into multiple pieces called shards. 

When you create an index, you can simply define the number of shards that you want.

###### Sharding is important for two primary reasons:
* It allows you to horizontally split/scale your content volume
* It allows you to distribute and parallelize operations across shards (potentially on multiple nodes) thus increasing performance/throughput

##### Replicas

In a network/cloud environment where failures can be expected anytime, it is very useful and highly recommended to have a failover mechanism in case a shard/node somehow crashes - Elasticsearch allows you to make one or more copies of your index’s shards into what are called replica shards, or replicas for short.

###### Replica is important for two primary reasons:
* It provides high availability in case a shard/node fails. For this reason, it is important to note that a replica shard is never allocated on the same node as the original/primary shard that it was copied from.
* It allows you to scale out your search volume/throughput since searches can be executed on all replicas in parallel.